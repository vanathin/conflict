package co.org.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/testController")
@Controller
public class TestController {

	@RequestMapping("/test")
	@ResponseBody
	public String test(){
		System.out.println("####");
		return "{key: value}";
	}

	
}
