package co.org.controller;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
//@Aspect
public class AdviceClass {

	@Pointcut("execution(void co.org.service.StudentService.dummy(..)) || execution(void co.org.*.*.*(..))")
//	@Pointcut("within(co.org.service.StudentService.*)")
	public void pointcut(){
		System.out.println("pointcut method");
	}
	@Before(value = "pointcut()")
	public void beforeAdvice(JoinPoint joinPoint,Object obj){
		System.out.println(joinPoint.getSignature().getName()+"#$#$#$" + obj);
		System.out.println("Before");
	}
	@After(value = "pointcut()")
	public void afterAdvice(){
		System.out.println("After");
	}
	@AfterReturning(value = "pointcut()")
	public void afterReturningAdvice(){
		System.out.println("AfterReturningAdvice");
	}
	@AfterThrowing(value="pointcut()")
	public void afterThrowingAdvice(){
		System.out.println("AfterThrowingAdvice");
	}
	@Around(value="pointcut()")
	public void aroundAdvice(ProceedingJoinPoint  p){
	System.out.println("Before around");
	try {
		p.proceed();
	} catch (Throwable e) {
		System.out.println(e.getMessage());
	}
	System.out.println("After around");
	
	}
	

}
